CC=g++

CFLAGS=-Ofast -march=native -mtune=native -std=c++14

LDFLAGS=

EXEC=main

SRC=./src/Format/CBitmap.cpp \
    ./src/Image/CImage.cpp   \
    ./src/Image/CLigne.cpp   \
    ./src/Image/CPixel.cpp   \
	./src/Forme/forme.cpp \
	./src/Forme/ligne.cpp \
	./src/Forme/point.cpp \
	./src/Forme/cercle.cpp \
	./src/Forme/cercleS.cpp \
	./src/Forme/carre.cpp \
	./src/Forme/carreS.cpp \
	./src/Forme/rectangle.cpp \
	./src/Forme/rectangleS.cpp \
	./src/Forme/triangleS.cpp \
	./src/Forme/poly.cpp \
	./src/Math/function.cpp \
    ./src/main.cpp

OBJ= $(SRC:.cpp=.o)

all: $(EXEC)

main: $(OBJ)
	$(CC) $(CFLAGS) -o ./bin/$@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: clean mrproper

clean:
	find ./bin -name main -exec rm {} \;
	find ./src -name *.o  -exec rm {} \;
	find ./bin -name *.bmp  -exec rm {} \;
	find ./ -name *.bmp  -exec rm {} \;

mrproper: clean
	rm $(EXEC)
