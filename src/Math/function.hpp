#ifndef _FUNCTION_
#define _FUNCTION_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
using namespace std;

namespace tools{
    
    int max(int a, int b);

    int min(int a, int b);

    int abs(int a, int b);

    bool is_on_the_left(int x1, int y1, int x2, int y2, int x, int y);
}
#endif

