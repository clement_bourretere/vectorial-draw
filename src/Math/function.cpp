
#include "function.hpp"

namespace tools{
    
    int max(int a, int b){
        return (a>b)?a:b ;
    }

    int min(int a, int b){
        return (a<b)?a:b ;
    }

    int abs(int a, int b){
        return ( (a>b)?(a-b):(b-a) );
    }

    bool is_on_the_left(int x1, int y1, int x2, int y2, int x, int y){
        if (x1 == x2){
            if (x > x1){ return false; }
            else { return true; }
        }
        if (y1 == y2){
            if (y > y1){ return true; }
            else {return false; }
        }
        else{
            float a = ((float)y2-(float)y1) / ((float)x2-(float)x1) ;
            float ya = (float)a * ((float)x-(float)x1) + (float)y1 ;

            if (y2 > y1){
                if (x2 > x1){
                    if (ya < y){ return true ;}
                    else { return false; }   
                }
                else{
                    if (ya > y){ return true ;}
                    else { return false; }
                }
            }   
            else {
                if (x2 > x1){
                    if (ya < y){ return true ;}
                    else { return false; }   
                }
                else{
                    if (ya > y){ return true ;}
                    else { return false; }
                }
            }
        }
    }
}