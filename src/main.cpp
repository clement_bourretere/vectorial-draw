#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>

#include "./Format/CBitmap.h"
#include "./Forme/ligne.hpp"
#include "./Forme/cercle.hpp"
#include "./Forme/cercleS.hpp"
#include "./Forme/carre.hpp"
#include "./Forme/carreS.hpp"
#include "./Forme/rectangle.hpp"
#include "./Forme/rectangleS.hpp"
#include "./Forme/poly.hpp"
#include "./Forme/triangleS.hpp"
#include "./Math/function.hpp"

using namespace std;
using namespace tools;





int main(int argc, char * argv[]) {

    #define fichier_vec argv[1]             // fichier_initial.vec
    #define fichier_bmp argv[2]             // fichier_final.bmp
    #define FE strtol(argv[3], NULL, 10)    // facteur d'échelle
    
    #define HAUTEUR 104*FE
    #define LARGEUR 104*FE

    cout << "(II) P_Bitmap exection start (" << __DATE__ << " - " << __TIME__ << ")" << endl;
    cout << "(II) + Number of arguments = " << argc << endl;
    cout << "(II) CBitmap object creation" << endl;
    CBitmap *image = new CBitmap();
    string filename2 = fichier_bmp;
    cout << "(II) CImage pointer extraction" << endl;

    CImage *img = new CImage(HAUTEUR, LARGEUR);
    
    
    string line;
    string name;
    string delimiter  = ",";
    string delimiter2 = ";";
    int _max = 10;
    vector<int> index;
    size_t pos = 0;
    ifstream myfile(fichier_vec);
    int X, Y, X2, Y2, X3, Y3, X4, Y4, H, L, C, trans, Z;
    string color;
    int test;



    if (myfile.is_open()){
        while (getline(myfile, line)){
            pos = 0;
            if (line[0] != '#'){
                cout << line << endl;
                pos = line.find(delimiter);
                if (pos < 20){
            // name
                    name = line.substr(0, pos);
                    line.erase(0, pos + delimiter.length());
                                        
                    // X
                    pos = line.find( delimiter);
                    X = stoi(line.substr(1, pos-1));
                    line.erase(0, pos +  delimiter.length());
                    // Y
                    pos = line.find( delimiter);
                    Y = stoi(line.substr(1, pos-1));
                    line.erase(0, pos +  delimiter.length());
                            
            // cercle
                    if (name=="CERCLE"){
                        // C
                        pos = line.find( delimiter);
                        C = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        cercle *_cercle = new cercle(X*FE, Y*FE, C*FE, color, trans, Z);
                        _cercle->tracer(img);
                        delete _cercle;
                    }


            // cercleS
                    if (name=="CERCLES"){
                        // C
                        pos = line.find( delimiter);
                        C = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        cercleS *_cercleS = new cercleS(X*FE, Y*FE, C*FE, color, trans, Z);
                        _cercleS->tracer(img);
                        delete _cercleS;
                    }

            // carré
                    if (name=="CARRE"){
                        // C
                        pos = line.find( delimiter);
                        C = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        carre *_carre = new carre(X*FE, Y*FE, C*FE, color, trans, Z);
                        _carre->tracer(img);
                        delete _carre;
                    }
                    
            // carréS
                    if (name=="CARRES"){
                        // C
                        pos = line.find( delimiter);
                        C = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        carreS *_carreS = new carreS(X*FE, Y*FE, C*FE, color, trans, Z);
                        _carreS->tracer(img);
                        delete _carreS;
                    }

            // ligne
                    if (name=="LIGNE"){
                        // X2
                        pos = line.find( delimiter);
                        X2 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Y2
                        pos = line.find( delimiter);
                        Y2 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        ligne *_ligne = new ligne(X*FE, Y*FE, X2*FE, Y2*FE, color, trans, Z);
                        _ligne->tracer(img);
                        delete _ligne;
                    }
                    
            // poly
                    if (name=="POLY"){
                        // X2
                        pos = line.find( delimiter);
                        X2 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Y2
                        pos = line.find( delimiter);
                        Y2 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // X3
                        pos = line.find( delimiter);
                        X3 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Y3
                        pos = line.find( delimiter);
                        Y3 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // X4
                        pos = line.find( delimiter);
                        X4 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Y4
                        pos = line.find( delimiter);
                        Y4 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        poly *_poly = new poly(X*FE, Y*FE, X2*FE, Y2*FE, X3*FE, Y3*FE, X4*FE, Y4*FE, color, trans, Z);
                        _poly->tracer(img);
                        delete _poly;
                    }

            // rectangle
                    if (name=="RECTANGLE"){
                        // L
                        pos = line.find( delimiter);
                        L = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // H
                        pos = line.find( delimiter);
                        H = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        rectangle *_rectangle = new rectangle(X*FE, Y*FE, L*FE, H*FE, color, trans, Z);
                        _rectangle->tracer(img);
                        delete _rectangle;
                    }

            // rectangleS
                    if (name=="RECTANGLES"){
                        // L
                        pos = line.find( delimiter);
                        L = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // H
                        pos = line.find( delimiter);
                        H = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        rectangleS *_rectangleS = new rectangleS(X*FE, Y*FE, L*FE, H*FE, color, trans, Z);
                        _rectangleS->tracer(img);
                        delete _rectangleS;
                    }

            // triangleS
                    if (name=="TRIANGLES"){
                        // X2
                        pos = line.find( delimiter);
                        X2 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Y2
                        pos = line.find( delimiter);
                        Y2 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // X3
                        pos = line.find( delimiter);
                        X3 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Y3
                        pos = line.find( delimiter);
                        Y3 = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // color
                        pos = line.find( delimiter);
                        color = line.substr(1, pos-1);
                        line.erase(0, pos +  delimiter.length());
                        // trans
                        pos = line.find( delimiter);
                        trans = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        // Z
                        pos = line.find( delimiter2);
                        Z = stoi(line.substr(1, pos-1));
                        line.erase(0, pos +  delimiter.length());
                        //tracé
                        triangleS *_triangleS = new triangleS(X*FE, Y*FE, X2*FE, Y2*FE, X3*FE, Y3*FE, color, trans, Z);
                        _triangleS->tracer(img);
                        delete _triangleS;
                    }



                }
            }
        }
    }
    myfile.close();



    image->setImage( img );
    cout << "(II) CBitmap image saving" << endl;
    image->SaveBMP(filename2);

    return(0);
}