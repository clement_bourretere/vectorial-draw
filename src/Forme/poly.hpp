#ifndef _POLY_
#define _POLY_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "ligne.hpp"
#include "point.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class poly : public forme{
    public:
        int X2;
        int Y2;
        int X3;
        int Y3;
        int X4;
        int Y4;
        poly(int _X1, int _Y1, int _X2, int _Y2, int _X3, int _Y3, int _X4, int _Y4, string _color, int _trans=100, int _Z=1);
        ~poly();
        void tracer(CImage *img);
};

#endif

