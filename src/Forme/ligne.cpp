
#include "ligne.hpp"


ligne::ligne(int _X, int _Y,int _X2, int _Y2, string _color, int _trans, int _Z) : forme( _X, _Y, _color, _trans, _Z ){
    X2 = _X2;
    Y2 = _Y2;
}
    
ligne::~ligne(){};

void ligne::tracer(CImage *img){
    int n = max(abs(X-X2), abs(Y-Y2))+1;
    int x = 0;
    int y = 0;
    for (int i=0 ; i<n ; i=i+1){
        x = X+i*(X2-X)/n;
        y = Y+i*(Y2-Y)/n;
        point *p = new point(x, y, color, trans, Z);
        p->tracer(img);
        delete p;
    }
    
}