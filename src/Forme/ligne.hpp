#ifndef _LIGNE_
#define _LIGNE_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "point.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class ligne : public forme{
    public :
        int X2;
        int Y2;
        ligne(int _X, int _Y, int _X2, int _Y2, string _color, int _trans = 100, int _Z = 1) ;
        ~ligne();
        void tracer(CImage *img);
};

#endif