
#include "rectangleS.hpp"

rectangleS::rectangleS(int _X, int _Y, int _L, int _H, string _color, int _trans, int _Z) : forme(_X, _Y,_color, _trans, _Z){
    L = _L;
    H = _H;
}


rectangleS::~rectangleS(){};


void rectangleS::tracer(CImage *img){
    int X2 = X + L;
    int Y2 = Y + H;
    for (int i=0; i<=tools::abs(Y2,Y); i++){
        ligne *l = new ligne(X, Y+i, X+tools::abs(X2,X), Y+i, color,trans, Z);
        l->tracer(img);
        delete l;
    }  
}