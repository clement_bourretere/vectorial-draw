#ifndef _TRIANGLES_
#define _TRIANGLES_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "point.hpp"
#include "ligne.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class triangleS : public forme{
    public:
        int X2;
        int Y2;
        int X3;
        int Y3;
        triangleS(int _X1, int _Y1, int _X2, int _Y2, int _X3, int _Y3, string _color, int _trans=100, int _Z=1);
        ~triangleS();
        void tracer(CImage *img);
};

#endif