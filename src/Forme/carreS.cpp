
#include "carreS.hpp"


carreS::carreS(int _X, int _Y, int _C, string _color, int _trans, int _Z) : forme( _X, _Y, _color, _trans, _Z ){
    C = _C;
}


carreS::~carreS(){};

void carreS::tracer(CImage *img){
    for (int i=0; i<=C; i++){
        ligne *l = new ligne(X, Y+i, X+C, Y+i, color, trans, Z);
        l->tracer(img);
        delete l;
    }  
}