#ifndef _CERCLES_
#define _CERCLES_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
using namespace std;

#include "point.hpp"

class cercleS: public forme{
    public:
        int R;
        cercleS(int _X, int _Y, int _R, string _color, int _trans = 100, int _Z = 1);
        ~cercleS();
        void tracer(CImage *img);
};

#endif