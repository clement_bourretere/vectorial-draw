
#include "point.hpp"

point::point(int _X, int _Y, string _color, int _trans, int _Z) : forme( _X, _Y, _color, _trans, _Z ){
}

point::~point(){};

void point::tracer(CImage *img){
    CPixel *p = img->getPixel(X, Y);
    int r;
    int g;
    int b;
    couleur(&r, &g, &b);
    //cout << p->Red() << " " << p->Green() << " " << p->Blue() << endl;
    r = ((100-trans)*p->Red() + trans*r)/100;
    g = ((100-trans)*p->Green() + trans*g)/100;
    b = ((100-trans)*p->Blue() + trans*b)/100;
    p->RGB(r,g,b);
}

