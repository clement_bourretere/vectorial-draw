
#include "poly.hpp"

poly::~poly(){}

poly::poly(int _X1, int _Y1, int _X2, int _Y2, int _X3, int _Y3, int _X4, int _Y4, string _color, int _trans, int _Z) : forme(_X1, _Y1, _color, _trans, _Z){
    X2 = _X2;
    Y2 = _Y2;
    X3 = _X3;
    Y3 = _Y3;
    X4 = _X4;
    Y4 = _Y4;
}

void poly::tracer(CImage *img){
    int _Xmin = tools::min(tools::min(X, X2), tools::min(X3, X4));
    int _Xmax = tools::max(tools::max(X, X2), tools::max(X3, X4));
    int _Ymin = tools::min(tools::min(Y, Y2), tools::min(Y3, Y4));
    int _Ymax = tools::max(tools::max(Y, Y2), tools::max(Y3, Y4));
   
    for (int x = _Xmin; x <= _Xmax; x++){
        for (int y = _Ymin; y <= _Ymax; y++){
            if ( not ((not tools::is_on_the_left( X, Y, X2, Y2, x, y)) || ( not tools::is_on_the_left( X2, Y2, X3, Y3, x, y)) || ( not tools::is_on_the_left( X3, Y3, X4, Y4, x, y)) || ( not tools::is_on_the_left( X4, Y4, X, Y, x, y)))){
                point *p = new point(x ,y, color, trans, Z);
                p->tracer(img);
                delete p;
            }
        }
    }
    
    
}