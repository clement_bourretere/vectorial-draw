#include "cercleS.hpp"

cercleS::cercleS(int _X, int _Y, int _R, string _color, int _trans, int _Z) : forme( _X, _Y, _color, _trans, _Z ){
    R = _R;
}

cercleS::~cercleS(){};

void cercleS::tracer(CImage *img){
    for(int x=X-R; x<=(X+R); x++){
        for(int y=Y-R; y<=(Y+R); y++){
            if ( ((x-X)*(x-X) + (y-Y)*(y-Y)) <= R*R ){
                point *p = new point(x, y, color, trans, Z);
                p->tracer(img);
                delete p;
            }
        }
    }
    
}