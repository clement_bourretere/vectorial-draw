#ifndef _CARRE_
#define _CARRE_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "../Math/function.hpp"

using namespace tools;
using namespace std;

#include "ligne.hpp"


class carre : public forme{
    public :
        int C;
        carre(int _X, int _Y, int _C, string _color, int _trans = 100, int _Z = 1) ;
        ~carre();
        void tracer(CImage *img);
};

#endif