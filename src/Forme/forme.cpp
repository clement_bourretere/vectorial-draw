
#include "forme.hpp"

forme::forme(int _X, int _Y, string _color, int _trans, int _Z){
    X = _X;
    Y = _Y;
    color = _color;
    trans = _trans;
    Z = _Z;
}


forme::~forme(){};


void forme::couleur(int *r, int*g, int* b){
    string line;
    ifstream myfile("./color/palette.txt");
    int _r = 0;
    int _g = 0;
    int _b = 0;
    int i = 0;
    if (myfile.is_open()){
        while (getline(myfile, line)){
            //cout << line << endl;
            if (color == line){
                //cout << line << endl;
                getline(myfile, line);
                //cout << line << endl;

                while(line[i]!=' '){
                    _r = _r*10 + ((int)(line[i]) - 48);
                    i++;
                }
                i++;

                while(line[i]!=' '){
                    _g = _g*10 + ((int)(line[i]) - 48);
                    i++;
                }
                i++;

                while(line[i]!=NULL){
                    _b = _b*10 + ((int)(line[i]) - 48);
                    i++;
                }
                i++;
            }
        }
    }
    else {cout << "Problem with color palette file opening" << endl;}
    *r = _r;
    *g = _g;
    *b = _b;
}
