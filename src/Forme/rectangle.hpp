#ifndef _RECTANGLE_
#define _RECTANGLE_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "ligne.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class rectangle: public forme{
    public:
        int L;
        int H;
        rectangle(int _X, int _Y, int _L, int _H, string _color, int _trans = 100, int _Z = 1);
        ~rectangle();
        void tracer(CImage *img);
};

#endif