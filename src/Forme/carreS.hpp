#ifndef _CARRES_
#define _CARRES_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "ligne.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;


class carreS : public forme{
    public :
        int C;
        carreS(int _X, int _Y, int _C, string _color, int _trans = 100, int _Z = 1) ;
        ~carreS();
        void tracer(CImage *img);
};

#endif