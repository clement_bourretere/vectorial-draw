#ifndef _POINT_
#define _POINT_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "../Image/CImage.h"
#include "forme.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class point : public forme{
    public :
        point(int _X, int _Y, string _color, int _trans, int _Z = 1) ;
        ~point();
        void tracer(CImage *img);
};

#endif

