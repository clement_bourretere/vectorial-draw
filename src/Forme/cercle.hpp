#ifndef _CERCLE_
#define _CERCLE_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "point.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class cercle: public forme{
    public:
        int R;
        cercle(int _X, int _Y, int _R, string _color, int _trans = 100, int _Z = 1);
        ~cercle();
        void tracer(CImage *img);
};

#endif