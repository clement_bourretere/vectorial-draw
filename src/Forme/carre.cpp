
#include "carre.hpp"


carre::carre(int _X, int _Y, int _C, string _color, int _trans, int _Z) : forme( _X, _Y, _color, _trans, _Z ){
    C = _C;
}

carre::~carre(){};


void carre::tracer(CImage *img){
    ligne *l1 = new ligne(X, Y, X+C, Y, color, trans, Z);
    l1->tracer(img);
    
    ligne *l2 = new ligne(X+C, Y, X+C, Y+C, color, trans, Z);
    l2->tracer(img);
    
    ligne *l3 = new ligne(X+C, Y+C, X, Y+C, color, trans, Z);
    l3->tracer(img);
    
    ligne *l4 = new ligne(X, Y+C, X, Y, color, trans, Z);
    l4->tracer(img);

    delete l1;
    delete l2;
    delete l3;
    delete l4;

}