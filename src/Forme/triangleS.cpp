#include "triangleS.hpp"

triangleS::~triangleS(){}

triangleS::triangleS(int _X1, int _Y1, int _X2, int _Y2, int _X3, int _Y3, string _color, int _trans, int _Z) : forme(_X1, _Y1, _color, _trans, _Z){
    X2 = _X2;
    Y2 = _Y2;
    X3 = _X3;
    Y3 = _Y3;
}

void triangleS::tracer(CImage *img){
    int _Xmin = tools::min( tools::min(X,X2), X3);
    int _Xmax = tools::max( tools::max(X,X2), X3);
    int _Ymin = tools::min( tools::min(Y,Y2), Y3);
    int _Ymax = tools::max( tools::max(Y,Y2), Y3);

    for (int x = _Xmin; x <= _Xmax; x++){
        for (int y = _Ymin; y <= _Ymax; y++){
            if (  (tools::is_on_the_left( X, Y, X2, Y2, x, y)) && (tools::is_on_the_left( X2, Y2, X3, Y3, x, y)) && (tools::is_on_the_left( X3, Y3, X, Y, x, y)) ){
                point *p = new point(x ,y, color, trans, Z);
                p->tracer(img);
                delete p;
            }
        }
    }
    
    ligne *l1 = new ligne(X,Y,X2,Y2,"Black");
    l1->tracer(img);
    delete l1;
    ligne *l2 = new ligne(X3,Y3,X2,Y2,"Black");
    l2->tracer(img);
    delete l2;
    ligne *l3 = new ligne(X,Y,X3,Y3,"Black");
    l3->tracer(img);
    delete l3;
}