
#include "rectangle.hpp"

rectangle::rectangle(int _X, int _Y, int _L, int _H, string _color, int _trans, int _Z) : forme( _X, _Y, _color, _trans, _Z ){
    L = _L;
    H = _H;
}

rectangle::~rectangle(){}

void rectangle::tracer(CImage *img){
    int X2 = X + L;
    int Y2 = Y + H;

    ligne *l1 = new ligne(X, Y, X, Y2, color, trans, Z) ;
    ligne *l2 = new ligne(X, Y, X2, Y, color, trans, Z) ;
    ligne *l3 = new ligne(X, Y2, X2, Y2, color, trans, Z) ;
    ligne *l4 = new ligne(X2, Y, X2, Y2, color, trans, Z) ;
    l1->tracer(img);
    l2->tracer(img);
    l3->tracer(img);
    l4->tracer(img);
    delete l1;
    delete l2;
    delete l3;
    delete l4;
    //ligne(X2, Y, X2, Y2, color) ;
}