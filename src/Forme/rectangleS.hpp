#ifndef _RECTANGLES_
#define _RECTANGLES_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "ligne.hpp"
#include "../Math/function.hpp"

using namespace std;
using namespace tools;


class rectangleS : public forme{
    public :
        int L;
        int H;
        rectangleS(int _X, int _Y, int _L, int _H, string _color, int _trans=100, int _Z=1) ;
        ~rectangleS();
        void tracer(CImage *img);
};

#endif