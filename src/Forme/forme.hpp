#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>

#include "../Math/function.hpp"

using namespace std;
using namespace tools;

class forme{
    public :
        int X ;
        int Y ;
        string color ;
        int trans ;
        int Z ;
        forme( int _X, int _Y, string _color, int _trans, int _Z = 1);
        ~forme();
        void couleur(int *r, int *g, int *b);
};


